<?php
/**
 * @file
 * This module implements various AddThis and ShareThis button sets in nodes'
 * $links variable, and in a block. 
 *
 * @todo Currently there's only a theme function implemented for the iframe
 *       version. We can add support for xfbml too.
 */


// Define module defaults:
define('re_social_media_HOSTS', '');
define('re_social_media_COUNTRY_CODES', 'en_US');


/**
 * Implements hook_block_info().
 */
function re_social_media_block_info() {
  $blocks['re_social_media_block'] = array(
    'info' => t('RE Social Media block'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
} // re_social_media_block_info()


/**
 * Implements hook_block_view().
 */
function re_social_media_block_view($delta = '') {
  if (variable_get('re_social_media_enable_block', FALSE)) {
    switch ($delta) {
      case 're_social_media_block':
        $block['subject'] = t('Share this');
        if (user_access('access re_social_media')) {
          $block['content'] = theme('re_social_media_buttons', array());
        }
        else {
          $block['content'] = '<p class="error">' . t('You do not have access to view this content.') . '</p>';        
        }
        break;
    }
    return $block;
  }
} // re_social_media_block_view()


/**
 * Implements hook_menu().
 */
function re_social_media_menu() {
  $items = array();
  $items['admin/config/services/re_social_media'] = array(
    'title' => 'RE Social Media Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('re_social_media_settings'),
    'access arguments' => array('administer re_social_media'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
} // re_social_media_menu()


/**
 * Implements hook_node_view().
 */
function re_social_media_node_view($node, $view_mode) {
  if ($view_mode != 'full'
    || !user_access('access re_social_media')
    || (!in_array($node->type, variable_get('re_social_media_node_types', array('page')), TRUE))) {
    return;
  }

  // Took this from the block.module...
  $pages = drupal_strtolower(variable_get('re_social_media_visibility', '<front>'));
  // Convert the Drupal path to lowercase
  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
  // Compare the lowercase internal and lowercase path alias (if any).
  $page_match = drupal_match_path($path, $pages);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
  }

  if (!$page_match) {
    $title = $node->title;
    return $node->content['links']['social_media'] = array(
      '#links' => array(
        array(
          'title' => theme('re_social_media_buttons', array()),
          'href' => NULL,
          'html' => TRUE, 
        ),
      ),
    );
  }
} // re_social_media_node_view()


/**
 * Implements hook_permission().
 */
function re_social_media_permission() {
 return array(
    'access re_social_media' => array(
      'title' => t('Access re_social_media'), 
      'description' => t('Access social media sharing links.'),
    ),
    'administer re_social_media' => array(
      'title' => t('Administer re_social_media'), 
      'description' => t('Administer social media sharing links.'),
    ),
  );
} // re_social_media_perm()


/**
 * Provides settings for the module.
 */
function re_social_media_settings() {
  $form['social_media_services'] = array(
    '#title' => t('Service'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['social_media_services']['re_social_media_service'] = array(
    '#type' => 'radios',
    '#title' => t('Choose a sharing service.'),
    '#options' => array('sharethis' => 'ShareThis', 'addthis' => 'AddThis'),
    '#default_value' => variable_get('re_social_media_service', array('sharethis')),
  );
  $form['social_media_services']['re_button_type'] = array(
    '#title' => t('Choose a button type'),
    '#type' => 'radios',
    '#options' => array(
      'bar' => t('Horizontal Bar (Only available on AddThis)') .'<br /><img src="https://s7.addthis.com/static/btn/v2/lg-share-en.gif" height="16" width="125">',
      'small' => t('Small square buttons') .'<br /><img src="http://sharethis.com/images/new/Button4.png" height="21" width="99">',
      'large' => t('Large square buttons') .'<br /><img src="http://sharethis.com/images/new/Button4.png">',
      'hcount' => t('Wide buttons, Horizontal counter') .'<br /><img src="http://sharethis.com/images/new/get-sharing-tools/HORZ.png">',
      'vcount' => t('Wide buttons, vertical counter (Only available on ShareThis)') .'<br /><img src="http://sharethis.com/images/new/get-sharing-tools/VERT.png">',
     ),
    '#default_value' => variable_get('re_button_type', array('large')),
  );
  $form['social_media_block'] = array(
    '#title' => t('Block Settings'),
    '#description' => t('RE Social Media can provide a block using the Button Type settings above. This is especially useful for use with the !display_suite and !context modules.', array('!display_suite' => l('Display Suite', 'https://drupal.org/project/ds'), '!context' => l('Context', 'https://drupal.org/project/context'))),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['social_media_block']['re_social_media_enable_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide social media links in a block'),
    '#default_value' => variable_get('re_social_media_enable_block', FALSE),
  );
  $form['social_media_services']['re_social_media_enable_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use with SSL'),
    '#default_value' => variable_get('re_social_media_enable_ssl', FALSE),
    '#description' => t("Enable to use ShareThis and AddThis with SSL."),
  );
  $form['social_media_settings'] = array(
    '#title' => t('General Settings'),
    '#description' => t('Choose which content types the social media buttons will appear on (the buttons will be output in the <code>$links</code> variable in the selected content types).'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['social_media_settings']['re_social_media_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('re_social_media_node_types', array('page')),
  );
  $form['social_media_settings']['re_social_media_visibility'] = array(
    '#type' => 'textarea',
    '#title' => t('Hide links on specific pages'),
    '#default_value' => variable_get('re_social_media_visibility', '<front>'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page."),
  );
  return system_settings_form($form);
} // re_social_media_settings()


/**
 * Implements hook_theme().
 */
function re_social_media_theme($existing, $type, $theme, $path) {
  // Theming functions for this module:
  return array(
    're_social_media_buttons' => array(
      'variables' => array(),
    ),
  );
} // re_social_media_theme()


/**
 * Theming function outputs the iframe version of the Facebook like button.
 *
 * @param array $variables Contains the path to the page and the page title
 * @return HTML The markup for the button
 * @see http://developers.facebook.com/docs/reference/plugins/like
 */
function theme_re_social_media_buttons(&$variables) {
  global $language;
  $service = variable_get('re_social_media_service', array('sharethis'));
  $button_type = variable_get('re_button_type', array('large'));
  $ssl - variable_get('re_social_media_enable_ssl', FALSE);
  $output = '';
  switch($service) {
    case 'sharethis': 
      if(module_exists('googleanalytics')) {
        $output .= '<script type="text/javascript">stLight.options({';
        $output .= 'publisherGA:"' . variable_get('googleanalytics_account', 'UA-') . '"';
        $output .= '});</script>';
      }
      ($button_type != 'small') ? $type = '_' . variable_get('re_button_type', array('large')) : $type = ''; 
      $output .=  "<!-- Share This Buttons BEGIN -->";
      $output .= '<script type="text/javascript">var switchTo5x=true;</script>';
      if($ssl == TRUE) {
        $output .= '<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>';
      } 
      else {
        $output .= '<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>';
      }
      $output .= '<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>';
      $output .= '<script type="text/javascript">stLight.options({publisher: "4e1667cf-8007-473e-96b5-3eda0d97f1fb", doNotHash: false, doNotCopy: false, hashAddressBar: false}); </script>';
      $output .= '<span class="st_facebook'. $type .'" displayText="Facebook"></span>';
      $output .= '<span class="st_twitter'. $type .'" displayText="Tweet"></span>';
      $output .= '<span class="st_googleplus'. $type .'" displayText="Google +""></span>';
      $output .= '<span class="st_email'. $type .'" displayText="Email"></span>';
      $output .= '<span class="st_sharethis'. $type .'" displayText="ShareThis"></span>';
      $output .= '<!-- Share This Buttons END -->';
      break;

    case 'addthis':
      $output = "<!-- AddThis Button BEGIN -->"; 
      $output .= '<script type="text/javascript">var addthis_config = { ';
      // Language choosing
      $output .= 'ui_language: "' . $language->language .'",';
      // Add Google Social Integration tracking  
      if(module_exists('googleanalytics')) {
        $output .= 'data_ga_property: "' . variable_get('googleanalytics_account', 'UA-') . '",';
        $output .= 'data_ga_social : true';
      }
      $output .= '};</script>';

      switch($button_type) {
        case 'bar':
          $output .= '<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=xa-518a847877bcbadf"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>';
        break;

        case 'small':
        case 'large':
          $output .= '<div class="addthis_toolbox addthis_default_style ';
          if($button_type == 'large') {
            $output .= 'addthis_32x32_style';
          } 
          $output .= '">';
          $output .= '<a class="addthis_button_facebook"></a>';
          $output .= '<a class="addthis_button_twitter"></a>';
          $output .= '<a class="addthis_button_google_plusone_share"></a>';
          $output .= '<a class="addthis_button_email"></a>';
          $output .= '<a class="addthis_button_compact"></a>';
          $output .= '<a class="addthis_counter addthis_bubble_style"></a>';
          $output .= '</div>';
        break;

        default:
          $output .= '<div class="addthis_toolbox addthis_default_style">';
          $output .= '<div class="addthis_toolbox ">';
          $output .= '<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>';
          $output .= '<a class="addthis_button_tweet"></a>';
          $output .= '<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>';
          $output .= '<a class="addthis_button_email"></a>';
          $output .= '<a class="addthis_counter addthis_pill_style"></a>';
          $output .= '</div>';
        break;
      }
      if($ssl == TRUE) {
        $output .= '<script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4f983a470fec5901"></script>';
      }
      else {
        $output .= '<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-4f983a470fec5901"></script>';
      }
      $output .= '<!-- AddThis Button END -->';
      break;
  }
  return $output;
} // theme_re_social_media_buttons()

